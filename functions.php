<?php

// Removes parent text domain
function remove_lang_setup() {
    remove_action('after_setup_theme','lang_setup');
}
add_action('after_setup_theme','remove_lang_setup');

// Add child text domain
function child_lang_setup(){
	load_theme_textdomain(NECTAR_THEME_NAME, get_stylesheet_directory() . '/lang');
}
add_action('after_setup_theme', 'child_lang_setup');

// Load styles avoiding @import
function enqueue_parent_style() {
    wp_enqueue_style( 'heading-font', 'http://fonts.googleapis.com/css?family=Patua+One' );
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'enqueue_parent_style' );

?>